package notnot;

public class Move {

    public boolean directions[] = new boolean[4];
    public long time;
    public String caption;

    public Move(boolean dir[], long time, String c) {
        this.directions = dir;
        this.time = time;
        this.caption = c;
    }

    public String toString() {
        String out = this.caption + "\n" + this.time + "\n";
        for (int i = 0; i < 4; i++) {
            out += "dir " + i + " - " + directions[i] + "\n";
        }
        return out;
    }

    public boolean check(int side) {
        if (side < 0 || side > 3) return false;
        return this.directions[side];
    }

    public boolean check() {
        if (directions[0] == false && directions[1] == false && directions[2] == false && directions[3] == false)
            return true;
        else
            return false;
    }
}
