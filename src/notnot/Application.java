
package notnot;

import com.thoughtworks.xstream.XStream;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author user
 */
public class Application extends JFrame implements Runnable {
    private static final int height = 700;
    private static final int width = 600;
    private JPanel loadPanel;
    private JLabel logoImg;
    private JLabel author;
    private final String logoPath = "res/img/logo.png";
    private final String movesPath = "res/moves/moves.xml";
    public final String leaderPath = "res/moves/bestScore.tbl";
    private final String musicPath = "res/sound/Onairstudio_Delusion_cut.wav";
    //private final String musicPath = "res/sound/malwareInjection.wav";
    private final String font = "res/Sony_Sketch_EF.ttf";

    private int best;

    public Clip soundTrack;

    public Application() {
        this.setTitle("NotNot");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(width, height);
        this.setLocationRelativeTo(null); //centered window
        loadPanel = new JPanel(new BorderLayout());
        loadPanel.setBackground(Color.black);
        this.setContentPane(loadPanel);
        this.setResizable(false);
        author = new JLabel("Luboshnikov Anton", JLabel.CENTER);
        author.setForeground(Color.WHITE);
        author.setFont(new Font("Sony Sketch EF", Font.PLAIN, 20));
        try {
            ImageIcon image = new ImageIcon(logoPath);
            logoImg = new JLabel("", image, JLabel.CENTER);
            loadPanel.add(logoImg, BorderLayout.CENTER);
            loadPanel.add(author, BorderLayout.SOUTH);
            loadPanel.setVisible(true);
        } catch (Exception e) {
            System.out.println(e);
        }

        try {
            BufferedReader rd = new BufferedReader(new FileReader(leaderPath));
            best = Integer.parseInt(rd.readLine());
        } catch (Exception ex) {
            File f = new File(leaderPath);
            try {
                f.createNewFile();
                BufferedWriter bw = new BufferedWriter(new FileWriter(leaderPath));
                bw.write(0 + "");
                bw.close();
            } catch (Exception ex1) {
                Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex1);
            }
            System.err.println(ex);
            best = 0;
        }


    }

    private void runTheGame() {
        synchronized (this) {
            try {
                wait(1500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        Game theGame = new Game(this, loadMoves(), best);
        theGame.run();
        this.setVisible(true);
        theGame.setVisible(true);
    }

    public void removeLogo() {
        this.logoImg.setVisible(false);
    }


    private ArrayList<Move> loadMoves() {
        XStream stream = new XStream();
        ArrayList<Move> moves = null;
        try {
            FileReader file = new FileReader(movesPath);
            stream.processAnnotations(Move.class);
            moves = (ArrayList<Move>) stream.fromXML(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return moves;
    }

    @Override
    public void run() {
        System.out.println("App is running");
        try {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File(font)));
        } catch (Exception e) {
        }
        new Application();

        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(musicPath));
            soundTrack = AudioSystem.getClip();
            soundTrack.open(audioInputStream);
            soundTrack.loop(Clip.LOOP_CONTINUOUSLY);
            soundTrack.start();
        } catch (Exception ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.setVisible(true);
        runTheGame();
    }
}
