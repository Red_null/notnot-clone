package notnot;

/**
 * @author Luboshnikov Anton
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //System.out.println("GDisk folder");
        Application app = new Application();
        app.run();

    }

}
