package notnot;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Game extends JPanel implements KeyListener, Runnable {
    private static Application app;

    public static final Color backClr = new Color(220, 220, 220);
    public static final Color fontClr = new Color(100, 100, 100); //100 100 100

    //option
    private static final int upKeyCode = 38;
    private static final int downKeyCode = 40;
    private static final int leftKeyCode = 37;
    private static final int rightKeyCode = 39;
    private static final int escKeyCode = 27;
    private static final int cheatKeyCode = 192;
    private final String pressMsg = "PRESS ANY KEY TO START";
    private final double diffOption = 1.4; // 1.5 - easy ; 0.1 - hard
    private int bestScore;
    private String leftAnimPath = "res/img/Anim-left.gif";
    private String upAnimPath = "res/img/Anim-up.gif";
    private String rightAnimPath = "res/img/Anim-right.gif";
    private String downAnimPath = "res/img/Anim-down.gif";
    private String gameOverAnimPath = "res/img/Anim-over.gif";
    private String waitAnimPath = "res/img/Anim-wait.gif";
    private String mutePath = "res/img/mute.png";
    private String unmutePath = "res/img/unmute.png";
    private String infoPath = "res/img/info.png";

    private JLabel msg;
    private JLabel moveCaption;
    private JPanel captionPanel;
    private JLabel animLabel;
    private JProgressBar countDown;
    private JLabel score;

    private ImageIcon leftAnim;
    private ImageIcon upAnim;
    private ImageIcon rightAnim;
    private ImageIcon downAnim;
    private ImageIcon gameOverAnim;
    private ImageIcon waitAnim;
    private ImageIcon empty = new ImageIcon("res/img/empty.png");
    private ImageIcon muteIcon;
    private ImageIcon unmuteIcon;
    private ImageIcon infoIcon;


    private boolean inProgress = false;
    private boolean ignoreKeys = false;
    private int step = -1;
    private long timeStart, timeOut, duration;
    private double difficult = diffOption;

    private Timer gameTimer = new Timer();
    private TimerTask timeTask;

    private JPanel gamePanel;
    private JPanel subPanel;
    private JPanel mutePanel;
    private JLabel muteLabel;
    private JLabel muteCapture;
    private JPanel infoPanel;
    private Move currentMove;
    private ArrayList<Move> moves;


    public Game(Application app, ArrayList<Move> moves, int best) {
        this.app = app;
        this.moves = moves;
        this.bestScore = best;
        System.out.println(best);

        msg = new JLabel(pressMsg, JLabel.CENTER);
        msg.setPreferredSize(new Dimension(200, 200));
        msg.setForeground(fontClr);
        msg.setFont(new Font("Sony Sketch EF", Font.PLAIN, 35));

        leftAnim = new ImageIcon(leftAnimPath);
        upAnim = new ImageIcon(upAnimPath);
        rightAnim = new ImageIcon(rightAnimPath);
        downAnim = new ImageIcon(downAnimPath);
        gameOverAnim = new ImageIcon(gameOverAnimPath);
        waitAnim = new ImageIcon(waitAnimPath);
        muteIcon = new ImageIcon(mutePath);
        unmuteIcon = new ImageIcon(unmutePath);
        infoIcon = new ImageIcon(infoPath);

        moveCaption = new JLabel("", JLabel.CENTER);
        moveCaption.setBounds(new Rectangle(500, 500));
        moveCaption.setPreferredSize(new Dimension(500, 500));
        moveCaption.setForeground(fontClr);
        moveCaption.setFont(new Font("Sony Sketch EF", Font.PLAIN, 50));

        score = new JLabel("Best score: " + bestScore, JLabel.CENTER);
        score.setPreferredSize(new Dimension(100, 100));
        score.setForeground(fontClr);
        score.setFont(new Font("Sony Sketch EF", Font.PLAIN, 30));


        countDown = new JProgressBar(0, 100);
        countDown.setPreferredSize(new Dimension(200, 50));
        countDown.setForeground(Color.gray);
        countDown.setBorderPainted(false);

        animLabel = new JLabel("", JLabel.CENTER);

        mutePanel = new JPanel(new BorderLayout());
        muteLabel = new JLabel("", unmuteIcon, JLabel.CENTER);
        muteCapture = new JLabel("M", JLabel.CENTER);
        muteCapture.setFont(new Font("Sony Sketch EF", Font.PLAIN, 20));
        mutePanel.add(muteLabel, BorderLayout.CENTER);
        mutePanel.add(muteCapture, BorderLayout.SOUTH);
        mutePanel.setBackground(backClr);
        mutePanel.setVisible(true);


        infoPanel = new JPanel(new BorderLayout());

        JLabel infolbl = new JLabel("", infoIcon, JLabel.CENTER);
        JLabel infoCap = new JLabel("I", JLabel.CENTER);
        infoPanel.setFont(new Font("Sony Sketch EF", Font.PLAIN, 20));

        infoPanel.add(infolbl, BorderLayout.CENTER);
        infoPanel.add(infoCap, BorderLayout.SOUTH);
        infoPanel.setBackground(backClr);
        infoPanel.setVisible(true);

        subPanel = new JPanel(new BorderLayout());
        subPanel.add(msg, BorderLayout.NORTH);
        subPanel.add(score, BorderLayout.CENTER);
        subPanel.add(mutePanel, BorderLayout.LINE_START);
        subPanel.add(infoPanel, BorderLayout.LINE_END);
        subPanel.setBackground(backClr);
        subPanel.setVisible(true);

        captionPanel = new JPanel(new BorderLayout());
        captionPanel.add(moveCaption, BorderLayout.CENTER);
        captionPanel.add(animLabel, BorderLayout.SOUTH);
        captionPanel.setBackground(backClr);
        captionPanel.setVisible(true);

        gamePanel = new JPanel(new BorderLayout());
        gamePanel.setBackground(backClr);
        gamePanel.add(captionPanel, BorderLayout.CENTER);
        gamePanel.add(countDown, BorderLayout.SOUTH);
        gamePanel.add(subPanel, BorderLayout.NORTH);

    }

    private void startGame() throws InterruptedException {
        step++;
        score.setText("" + step);
        if (step % 2 == 0 && difficult > 0.3)
            difficult -= 0.1;
        currentMove = moves.get((((Double) (Math.random() * 100 % moves.size())).intValue()));
        moveCaption.setText(currentMove.caption);
        timeStart = System.currentTimeMillis();
        timeOut = (long) (timeStart + (currentMove.time * difficult)) + 50;
        duration = timeOut - timeStart;
        if (duration < 600) duration = 650;
        // System.out.println("Step: "+step+"Diff: "+difficult+" Duration: "+duration);
        gameTimer = new Timer();
        timeTask = new TimerTask() {
            @Override
            public void run() {
                long currTime = System.currentTimeMillis();
                changeCountdown((int) (((timeOut - currTime) * 100) / duration));
                if (currTime > timeOut && currentMove.check()) {
                    this.cancel();
                    try {
                        startGame();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    return;
                }
                if (currTime > timeOut) {
                    gameOver();
                }
            }
        };
        gameTimer.schedule(timeTask, 0, 50);
    }

    public void changeCountdown(int val) {
        if (val <= 0) {
            try {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        countDown.setValue(0);
                    }
                });
                java.lang.Thread.sleep(1);
            } catch (InterruptedException e) {
            }
            return;
        }

        try {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    countDown.setValue(val);
                }
            });
            java.lang.Thread.sleep(100);
        } catch (InterruptedException e) {
        }

    }

    public void gameOver() {
        Thread gmOver = new Thread() {
            public void run() {
                synchronized (this) {
                    gameTimer.cancel();
                    timeTask.cancel();
                    inProgress = false;

                    anim(gameOverAnim);

                    moveCaption.setText("Game over!");

                    if (step > bestScore) {
                        score.setText("New Record: " + step);
                        bestScore = step;
                        try {
                            BufferedWriter bw = new BufferedWriter(new FileWriter(app.leaderPath));
                            bw.write(step + "");
                            bw.close();
                        } catch (IOException ex) {
                            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    } else
                        score.setText("Your score: " + step);

                    ignoreKeys = true;
                    step = -1;
                    difficult = diffOption;
                    countDown.setValue(0);
                    gamePanel.repaint();
                    try {
                        wait(2000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    ignoreKeys = false;
                    msg.setText(pressMsg);
                    moveCaption.setText("");
                    score.setText("Best score: " + bestScore);
                    anim(waitAnim);
                    msg.setVisible(true);
                    gamePanel.repaint();
                }
            }
        };
        gmOver.start();
    }

    public void muteSound() {
        if (app.soundTrack.isActive()) {
            try {
                app.soundTrack.stop();
                muteLabel.setIcon(muteIcon);
            } catch (Exception e) {
            }
        } else {
            try {
                app.soundTrack.start();
                muteLabel.setIcon(unmuteIcon);
            } catch (Exception e) {
            }
        }
    }

    public void showInfo() {
        JDialog info = new JDialog();
        info.setLocationRelativeTo(null);
        info.setLocation(app.getLocation());
        info.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                info.setVisible(false);
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
            }
        });
        info.setSize(600, 400);
        info.setLayout(new GridLayout(3, 1));
        JLabel cap = new JLabel("Developer: Luboshnikov Anton", JLabel.CENTER);
        JLabel cap1 = new JLabel("Images: pixabay.com", JLabel.CENTER);
        JLabel cap2 = new JLabel("Music: Onairstudio - Delusion", JLabel.CENTER);

        cap.setFont(new Font("Sony Sketch EF", Font.PLAIN, 30));
        cap1.setFont(new Font("Sony Sketch EF", Font.PLAIN, 30));
        cap2.setFont(new Font("Sony Sketch EF", Font.PLAIN, 30));
        info.add(cap);
        info.add(cap1);
        info.add(cap2);
        info.setVisible(true);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //System.out.println(e.getKeyCode());
        if (!ignoreKeys) {
            if (e.getKeyCode() == 77) {
                muteSound();
                return;
            }
            if (!inProgress) {
                if (e.getKeyCode() == 73) {
                    showInfo();
                    return;
                }
                msg.setVisible(false);
                animLabel.setIcon(empty);
                inProgress = true;
                try {
                    startGame();
                } catch (InterruptedException ex) {
                    Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                animLabel.setIcon(empty);
                //UP
                if (e.getKeyCode() == upKeyCode) {
                    if (currentMove.check(1)) {
                        gameTimer.cancel();
                        anim(upAnim);
                        try {
                            startGame();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        gameOver();
                    }
                    return;
                }
                // DOWN
                if (e.getKeyCode() == downKeyCode) {
                    if (currentMove.check(3)) {
                        gameTimer.cancel();
                        anim(downAnim);
                        try {
                            startGame();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        gameOver();
                    }
                    return;
                }

                //LEFT
                if (e.getKeyCode() == leftKeyCode) {

                    if (currentMove.check(0)) {
                        gameTimer.cancel();
                        anim(leftAnim);
                        try {
                            startGame();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        gameOver();
                    }
                    return;
                }

                //RIGHT
                if (e.getKeyCode() == rightKeyCode) {
                    if (currentMove.check(2)) {
                        gameTimer.cancel();
                        anim(rightAnim);

                        try {
                            startGame();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        gameOver();
                    }
                    return;
                }
                //CHEAT
                if (e.getKeyCode() == cheatKeyCode) {

                    gameTimer.cancel();
                    try {
                        startGame();
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return;
            }
            //EXIT
            if (e.getKeyCode() == escKeyCode) System.exit(0);
        }
    }

    public void anim(ImageIcon anim) {
        anim.getImage().flush();
        animLabel.setIcon(anim);
    }

    @Override
    public void run() {
        System.out.println("Game is running");
        app.removeLogo();
        app.setFocusable(true);
        app.addKeyListener(this);
        gamePanel.setVisible(true);
        app.setContentPane(gamePanel);
        anim(waitAnim);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
